package com.ustglobal.itopsreport.test;



import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.ustglobal.itopsreport.bean.AuditWireTapProcessor;


@Component
public class MemberQuery extends RouteBuilder {

	
	
	AuditWireTapProcessor auditWireTapProcessor = new AuditWireTapProcessor();
	@Value("${MEMBER_QUERY}")
	private String memberQuery;
	String[] arr= {"PROCESS_DATE", "STATUS", "COUNT"};

	
	@Override
	public void configure()  throws Exception{
		
		from("timer://member_Timer?repeatCount=1")
		.routeId("MEMBER_RECONCILLATION")
		.log("member_Query Started @ :: ${date:now:yyyy-MM-dd - HH:mm:ss}")
		.to("sql:"+memberQuery)
		.log("Member Query: ${body}")
		.marshal(new CsvDataFormat().setDelimiter(',').setAllowMissingColumnNames(true).setHeader(arr))
		.to("file:{{output.path.QueryReport.Member_Provider}}?fileName=member_query.csv")
		.log("Member Query got Executed in the Local Path")
		.setHeader("To", simple("{{spring.mail.properties.mail.to.email}}"))
		.setHeader("From", simple("{{spring.mail.properties.mail.from.email}}"))
		.log("Mail Sent for  Member Query Details ")
		.end();

		
		
	
	}

}

package com.ustglobal.itopsreport.test;



import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.ustglobal.itopsreport.bean.AuditWireTapProcessor;
import com.ustglobal.itopsreport.bean.EmailService;



@Component
public class ProviderQuery extends RouteBuilder {
	
	AuditWireTapProcessor auditWireTapProcessor = new AuditWireTapProcessor();
	@Value("${PROVIDER_QUERY}")
	private String providerQuery;
	String[] arr= {"RECEIVED_DATE", "FILE_TYPE", "COUNT"};
	
	
	
	
	@Override
	public void configure()  throws Exception{
		
		from("timer://provider_Timer?repeatCount=1")
		.routeId("PROVIDER_RECONCILLATION")
		.log("provider_Query Started @ :: ${date:now:yyyy-MM-dd - HH:mm:ss}")
		.to("sql:"+providerQuery)
		.log("Provider Query: ${body}")
		.marshal(new CsvDataFormat().setDelimiter(',').setAllowMissingColumnNames(true).setHeader(arr))
		.to("file:{{output.path.QueryReport.Member_Provider}}?fileName=provider_query.csv")
		.log("Provider Query got Executed in the Local Path")
		.setHeader("To", simple("{{spring.mail.properties.mail.to.email}}"))
		.setHeader("From", simple("{{spring.mail.properties.mail.from.email}}"))
		.bean(EmailService.class, "attachFileToExchange")
		.log("Mail Sent for  Member&Provider Query  ")
		.end();
		
	
				
	
	
	
	}

}
